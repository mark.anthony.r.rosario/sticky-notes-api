<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Auth;
use Pusher;
class TasksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    
    public function getAllTasks()
    {
      $tasks = Redis::smembers('tasks');
      return response()->json($tasks);
    }
  
    public function clearAllTasks()
    {
      //delete all 
      Redis::flushall();
      $this->broadcast();
      echo json_encode(['message' => 'tasks cleared!']);
    }
  
    public function createTask(Request $request)
    {
      Redis::sadd('tasks',$request->input('taskName'));
      $this->broadcast();
      echo json_encode(['message' => 'task saved!']);
    }
    private function broadcast()
    {
       
        $options = array(
    'encrypted' => true
  );
  $pusher = new Pusher(
    'eaeb26d5342387203669',
    '009907d1b1daa0cc6da4',
    '309874',
    $options
  );

  $data['message'] = 'hello world';
  $pusher->trigger('task-channel', 'task-update', $data);  
    }
    public function cors()
    {
      //this action is required because the http verb OPTIONS called by js "prefligt"
      //prior to DELETE needs a way to get the middleware's response
    }
      
    
}
