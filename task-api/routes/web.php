<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');

});

Route::get('/x', function () {
    echo "xxx";
});

Route::group(['prefix' => 'tasks' ,'middleware' => 'cors'], function () {
    Route::get('/', 'TasksController@getAllTasks');
    Route::post('/', 'TasksController@createTask');
    Route::delete('/', 'TasksController@clearAllTasks');
    Route::options('/', 'TasksController@cors');
});
