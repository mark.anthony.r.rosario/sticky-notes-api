# Deployment

## make a droplet (one click app)
- make LEMP on ubuntu 16.04 
- install your deployHQ.com ssh key
- install your other ssh keys
- login using root and run mysql_secure_installation to finish setting up the server
- install phpmbstring
```
apt-get zip
apt-get install php-mbstring php7.0-mbstring php-gettext libapache2-mod-php7.0
```

## make a new user "deployer" with SSH access
- make new userhttps://www.digitalocean.com/community/tutorials/how-to-create-a-sudo-user-on-ubuntu-quickstart
- make ssh keys 
```
#login as user "deployer" and generate your keys
#https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2
ssh-keygen -t rsa
```
- copy your deployHQ.com public key to your deployer's ~/.ssh/authorized_keys file https://<your-domain>.deployhq.com/projects/taskboard/public_key
- login as "deployer" (su - deployer) and make /var/www/html writable 
```
chmod 777 /var/www/html -R
```
## fix webroot path
```
#requires service nginx restart
vi /etc/nginx/sites-enabled/digitalocean
server {
...
root /var/www/html/task-api/public;
...
location /{
    ...
    try_files   $uri $uri/ /index.php?$query_string;
    ...
}
}
```

## change server's hostname at deployhq.com

- replace with the hostname of your new droplet
- add firewall access 
```
ufw allow from <deployhq ip range>
```



## enable swap for composer update problems
```
/bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
/sbin/mkswap /var/swap.1
sudo /sbin/swapon /var/swap.1
```

## deploy using deployhq.com!



#update heroku API_URL config var
- https://dashboard.heroku.com/apps/sticky-notes-2017/settings